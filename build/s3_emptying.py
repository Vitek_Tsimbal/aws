from boto.s3.connection import S3Connection, Bucket, Key
import os
import sys
import argparse
import boto3
import botocore

def emptying_bucket(accessKey, accessSecretKey, bucketName):
  print('Trying to connect to bucket')
  try:
    s3 = boto3.client('s3',
         aws_access_key_id=accessKey,
         aws_secret_access_key=accessSecretKey)
    print('Trying to get list objects from bucket')
    try:
      response = s3.list_objects(
        Bucket=bucketName
      )
      if (response.get('Contents') != None):
        print('Trying to empty bucket')
        try:
          for element in response.get('Contents'):
            s3.delete_object(
              Bucket=bucketName,
              Key=element.get('Key')
            )
          print('Bucket successfull emptied')
          return True
        except BaseException as err:
          print(err)
          return False
      else:
        return True
        print('Bucket is empty')
    except BaseException as err:
      print(err)
      return False
  except BaseException as err:
    print('Some error happend, when connecting to bucket')
    print(err)
    return False

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('accessKey', help="Access key to bucket")
    parser.add_argument('accessSecretKey', help="Secret key to bucket")
    parser.add_argument("bucketName", help="Bucket name")
    args = parser.parse_args()

    if not emptying_bucket(args.accessKey, args.accessSecretKey, args.bucketName):
        sys.exit(1)

if __name__ == "__main__":
    main()