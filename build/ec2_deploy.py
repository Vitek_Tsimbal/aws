import paramiko, sys, argparse, os


def connectToSSH(pathToPrivateKey):
  try:
    print('Create SSH Client')
    ssh = paramiko.SSHClient()
    print('Accept policy')
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    host = 'ec2-18-221-251-190.us-east-2.compute.amazonaws.com'
    print('Connecting by ssh to ec2 server')
    ssh.connect(host, username='ec2-user', password='password', key_filename=pathToPrivateKey)
    print('Connected to ssh')
    print('Getting filelist')
    stdin, stdout, stderr = ssh.exec_command('ls')
    print(stdout.readlines())
    # ssh.close()
    try:
      print('Open SFTP for transfer files')
      sftp = ssh.open_sftp()
      try:
        print('Getting filelist for deleting')
        filesInRemoteArtifacts = sftp.listdir(path='./')
        print('Trying to delete getted files')
        print('Files in remote machine')
        print(filesInRemoteArtifacts)
        if 'site.zip' in filesInRemoteArtifacts:
          print('Remove previous site zipped version')
          sftp.remove('site.zip')
        if 'unzip.sh' in filesInRemoteArtifacts:
          print('Delete old bash script')
          sftp.remove('unzip.sh')
        try:
          print(os.listdir('./'))
          print('Putting files to server')
          sftp.put('./content.zip', './content.zip')
          sftp.put('./build/sh/unzip.sh', './unzip.sh')
          sftp.put('./build/unpack_content.py', './unpack_content.py')
          print('Getting filelist')
          stdin, stdout, stderr = ssh.exec_command('ls')
          print(stdout.readlines())
          print('SUCCESS UPDATED SERVER')
          try:
            print('Trying to run bash scripts on EC2 server')
            # stdin, stdout, stderr = ssh.exec_command('sh unzip.sh')
            print('Unzipping content')
            stdin, stdout, stderr = ssh.exec_command('python unpack_content.py')
            print('Success unzipped')
            stdin, stdout, stderr = ssh.exec_command('ls')
            # print('Running node server')
            # stdin, stdout, stderr = ssh.exec_command('python run_server.py')
            print('Close ssh connection')
            ssh.close()
            print('Connection closed')
            return True
          except BaseException as err:
            print('Some error went wrong when trying to run bash scripts on EC2 server')
            print(err)
            print('Close ssh connection')
            ssh.close()
            print('Connection closed')
            return False
        except BaseException as err:
          print('Something went wrong with putting files to ec2 server')
          print(err)
          print('Close ssh connection')
          ssh.close()
          print('Connection closed')
          return False
      except BaseException as err:
        print('Something went wrong with emptying ec2 server')
        print(err)
        print('Close ssh connection')
        ssh.close()
        print('Connection closed')
        return False
    except BaseException as err:
      print('Something went wrong with transfering files')
      print(err)
      print('Close ssh connection')
      ssh.close()
      print('Connection closed')
      return False
  except BaseException as err:
    print('Something went wrong')
    print(err)
    print('Close ssh connection')
    ssh.close()
    print('Connection closed')
    return False

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('pathToPrivateKey', help="Path to private key")
  args = parser.parse_args()

  if not connectToSSH(args.pathToPrivateKey):
    sys.exit(1)

if __name__ == "__main__":
    main()